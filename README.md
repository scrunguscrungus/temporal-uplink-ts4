## Temporal Uplink TS4

Tools for working with the file formats of the TimeSplitters 4 prototype build.

Right now, this is just an unpacker for the .PAK files. Perhaps in the future we will be able to repack them and do other things, who knows?

# ts4_unpak_pub
Place this in either `TEST01236/USRDIR/pak/cell` or `TEST01236/USRDIR/pak/xenon`. Cell is the PS3, Xenon is the Xbox 360. Run and all files will be extracted with correct dates and filenames to `fsys.pak_extracted`