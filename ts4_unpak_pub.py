# TS4 PAK unpacker
# By Jill
# Can't believe I'm writing this tool

#import simplejson as json #Because big Python doesn't want us to export bytes for some stupid reason...
import dataclasses
from typing import BinaryIO
from typing import List
import os
import struct
from datetime import datetime,timedelta
from dataclasses import dataclass

@dataclass
class PakHeader:
	magic: bytes
	fileTableLen: int
	unknown: int #Probably compressed length as in TS3
	chunks : int
	unknown_also: int #?

@dataclass
class PakFileEntry:
	size: int
	compressedSize: int
	chunkPos: int
	unk: int
	pad : int
	chunk : int

@dataclass
class PakFile:
	header : PakHeader
	filetable : List[PakFileEntry]

@dataclass
class PakStringData:
	md5: str
	timestamp: int
	offset: int
	generic: int
	flags: int

PAK_HEADER : PakHeader

def readInt(file: BinaryIO) -> int:
	return int.from_bytes(file.read(4), 'big');

def readPackHeader(file : BinaryIO) -> PakHeader:
	print("Reading header...");
	magic = file.read(4)
	if ( magic.decode('utf-8') != "PCK2" and magic.decode('utf-8') != "PCK1" ):
		print("Pack isn't a pack?")
		return None
	
	data = file.read(12)
	tableLen, unknown, chunks, seed = struct.unpack(">IIHH", data)
	return PakHeader(magic, tableLen, unknown, chunks, seed)

def readPackFileEntry(file : BinaryIO) -> PakFileEntry:
	size = readInt(file)
	compressedSize = readInt(file) #Probably.
	chunkPos = readInt(file)
	unk = int.from_bytes(file.read(3), 'big') #Not sure
	padchunk = int.from_bytes(file.read(1), 'big')
	pad = padchunk & 0x80
	chunk = padchunk & 0x7f
	return PakFileEntry(size, compressedSize, chunkPos, unk, pad, chunk)

def readPackFileTable(file : BinaryIO) -> List[PakFileEntry]:
	print("Reading file table...");
	numFiles : int = PAK_HEADER.fileTableLen
	files = []
	for i in range(numFiles):
		files.append(readPackFileEntry(file))
	return files

def readSinglePackStringData(file : BinaryIO) -> PakStringData:
	md5 = file.read(16).hex();
	timestamp = int.from_bytes(file.read(8), 'big')
	offset = readInt(file)
	generic = int.from_bytes(file.read(3), 'big')
	flags = int.from_bytes(file.read(1), 'big')
	return PakStringData(md5, timestamp, offset, generic, flags)


def readPackStringData(file : BinaryIO) -> List[PakStringData]:
	numFiles : int = PAK_HEADER.fileTableLen
	data = []
	for i in range(numFiles):
		data.append(readSinglePackStringData(file))
	return data

#This is probably super inefficient, I do not care :sleep:
def getString(offset: int, allStrings: bytes):
	slice = allStrings[offset:]
	return slice.split(b'\x00')[0].decode('utf-8')

#https://stackoverflow.com/a/38880683
EPOCH_AS_FILETIME = 116444736000000000  # January 1, 1970 as MS file time
HUNDREDS_OF_NANOSECONDS = 10000000
def filetime_to_dt(ft):
    us = (ft - EPOCH_AS_FILETIME) // 10
    return datetime(1970, 1, 1) + timedelta(microseconds = us)


#if ( len(sys.argv) < 3 ):
#	print("Usage: ts4_cellunpak.py [.pak file] [out path] [optional: -v for verbose output]")

#FILENAME = sys.argv[1]
FILENAME = "fsys.pak"
STRINGTABLE = FILENAME+".str"
#OUTPATH = sys.argv[2]
OUTPATH = "fsys.pak_extracted"
VERBOSE = False
#if ( len(sys.argv) > 3 ):
#	if ( sys.argv[3] == "-v"):
#		VERBOSE = True


#I don't handle compressed files (which are presumably supported, since TS3 has them)
#because there are none in TS4
def getFileData(pak : PakFile, idx : int) -> bytes:
	fileInfo = pak.filetable[idx]
	chunk = fileInfo.chunk
	chunkname = f"{FILENAME}.{chunk:02d}"
	if ( VERBOSE ):
		print(f"File is in chunk {chunkname}")
	
	chunkfile = open(chunkname, "rb")
	chunkfile.seek(fileInfo.chunkPos, 0)

	data = chunkfile.read(fileInfo.size)
	chunkfile.close()
	return data


file = open(FILENAME, "rb");
stringsfile = open(STRINGTABLE, "rb")

print("Reading main PAK file...")
PAK_HEADER = readPackHeader(file)
#print(PAK_HEADER)
files = readPackFileTable(file)
#print(files)

pakfile = PakFile(PAK_HEADER, files)

#debugOut = open("unpak_debug.json", "w")
#json.dump(pakfile, debugOut, indent=1, cls=thisShouldBeDefault)
#debugOut.close()

#print(file.tell())

print("Reading strings pack file...")
stringData = readPackStringData(stringsfile)

#debugOut = open("unpak_debug_stringtable.json", "w")
#json.dump(stringData, debugOut, indent=1, cls=thisShouldBeDefault)
#debugOut.close()

pos = stringsfile.tell()
stringsfile.seek(0, 2)
eof = stringsfile.tell()
remaining = eof - pos
stringsfile.seek(pos, 0)
allStrings = stringsfile.read(remaining) #Read this as bytes because PakStringData.offset is a direct byte-based offset into it. Never change FRD (oh wait, you didn't)

#Finally, we now have all the info we need to read files

print("Extracting files, this may take a bit...")
i : int = 0
for file in pakfile.filetable:
	fileName = os.path.join(OUTPATH, getString(stringData[i].offset, allStrings))
	
	dir = os.path.dirname(fileName)
	if ( not os.path.exists(dir) ):
		os.makedirs(dir)

	#extractedFile = open(fileName, "w")
	#extractedFile.close()

	if ( VERBOSE ):
		print(f"Extracting file {i} to {fileName}...")
	data = getFileData(pakfile, i)

	try:
		extracted = open(fileName, "wb")
		extracted.write(data)
		extracted.close()
	except Exception as e:
		print("Error!")
		print(repr(e))
		input("Press Enter to fail :(")
		exit()

	fileTime = filetime_to_dt(stringData[i].timestamp).timestamp()
	os.utime(fileName, (fileTime, fileTime))

	i = i+1

print("Done!")